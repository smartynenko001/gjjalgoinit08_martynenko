package main.java.com.getjavajob.training.algo.util;

import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by Sergey Martynenko.
 */
public class Assert {

        public static void assertEquals(String testName, boolean expected, boolean actual) {
            if (expected == actual) {
                System.out.println(testName + " passed");
            } else {
                System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
            }
        }

        public static void assertEquals(String testName, double expected, double actual) {
            if (expected == actual) {
                System.out.println(testName + " passed");
            } else {
                System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
            }
        }

        public static void assertEquals(String testName, int expected, int actual) {
            if (expected == actual) {
                System.out.println(testName + " passed");
            } else {
                System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
            }
        }

        public static void assertEquals(String testName, String expected, String actual) {
            if (expected.compareTo(actual)==0) {
                System.out.println(testName + " passed");
            } else {
                System.out.println(testName + " failed: expected " + expected + ", actual " + actual);
            }
        }

        public static void assertEquals(String testName, int[] expected, int[] actual) {
            if (Arrays.equals(expected,actual)) {
                System.out.println(testName + " passed");
            } else {
                System.out.println(testName + " failed: expected:\n " +
                        Arrays.toString(expected)+",\n actual:\n " + Arrays.toString(actual));
            }
        }

        public static void assertEquals(String testName, int[][] expected, int[][] actual) {
            if (Arrays.deepEquals(expected,actual)) {
                System.out.println(testName + " passed");
            } else {
                System.out.println(testName + " failed: expected:\n " +
                        Arrays.deepToString(expected)+",\n actual:\n " + Arrays.deepToString(actual));
            }
        }

        public static void assertEquals(String testName, ArrayList<?> expected, ArrayList<?> actual) {
            if (!expected.retainAll(actual)&&!actual.retainAll(expected)) {
                System.out.println(testName + " passed");
            } else {
                System.out.println(testName + " failed: expected:\n " +
                        expected.toString()+",\n actual:\n " + actual.toString());
            }
        }

    }
