package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh02N039.getHourHandAngle;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh02N039Test {

    public static void main(String[] args) {
        testGetHourHandAngle();
    }

    private static void testGetHourHandAngle() {
        final int TEST_HOUR=0;
        final int TEST_MINUTE=20;
        final int TEST_SECOND=0;
        final int FINAL_ANGLE=10;
        assertEquals("TaskCh02N039Test.getHourHandAngle",FINAL_ANGLE,getHourHandAngle(TEST_HOUR,TEST_MINUTE,TEST_SECOND));
    }

}
