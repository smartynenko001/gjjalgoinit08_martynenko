package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh04N015 {

    public static int calculateAge(int yearOfBirth, int monthNumberOfBirth, int yearToday, int monthNumberToday) {
        if(monthNumberOfBirth>monthNumberToday) {
          return yearToday-yearOfBirth-1;
        } else {
          return yearToday-yearOfBirth;
        }
    }

}
