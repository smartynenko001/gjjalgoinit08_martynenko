package main.java.com.getjavajob.training.algo.init.martynenkos;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh13N012 {

    public static void main(String args[]){
        Database myCompany=new Database();
        myCompany.addEmployee(new Employee("Ivan","Ivanovich","Ivanov","Moscow",2000,3));
        myCompany.addEmployee(new Employee("Semen","Ivanovich","Ivanov","Naryan-Mar",2016,3));
        myCompany.addEmployee(new Employee("Jose","Maria","Rodriguez","Santa Clara",2005,12));
        myCompany.addEmployee(new Employee("Feng","Lee","Makao",2012,1));
        myCompany.printStaffData(myCompany.searchBySeniority(3));
    }

}

class Employee {

    private final String name;
    private String patronymic;
    private final String lastName;
    private final String address;
    private final Calendar enrollDate;

    Employee(String name, String patronymic, String lastName, String address, int startYear, int startMonth){
        this.name=name;
        this.patronymic=patronymic;
        this.lastName=lastName;
        this.address=address;
        this.enrollDate=new GregorianCalendar(startYear,startMonth-1,1);
    }

    Employee(String name, String lastName, String address, int startYear, int startMonth){
        this.name=name;
        this.lastName=lastName;
        this.address=address;
        this.enrollDate=new GregorianCalendar(startYear,startMonth,1);
    }

    private String getName(){
        return name;
    }

    private String getPatronymic(){
        return (patronymic!=null)?patronymic:"-";
    }

    private String getLastName(){
        return lastName;
    }

    private String getAddress(){
        return address;
    }

    private String getEnrollDate(){
        return Integer.toString(enrollDate.get(Calendar.MONTH))+"."+Integer.toString(enrollDate.get(Calendar.YEAR));
    }

    private double getSeniority(){
        Calendar currentDate=new GregorianCalendar();
        return currentDate.get(Calendar.YEAR)-enrollDate.get(Calendar.YEAR)+1+
                (currentDate.get(Calendar.MONTH)-enrollDate.get(Calendar.MONTH)+1)/12;
    }

    boolean hasEnoughExperience(int years){
        return (this.getSeniority()>=years);
    }

    String getFullData(){
        return getName()+" "+getPatronymic()+" "+getLastName()+" lives at:"+getAddress()+" has been working since "+getEnrollDate();
    }

    boolean myNameIsLike(String nameFragment){
        return getName().toLowerCase().contains(nameFragment.toLowerCase())||
                getLastName().toLowerCase().contains(nameFragment.toLowerCase())||
                getPatronymic().toLowerCase().contains(nameFragment.toLowerCase());
    }

}

 class Database{

     private final ArrayList<Employee> staff;

    public Database(){
        int STAFF_COUNT = 20;
        staff= new ArrayList<>(STAFF_COUNT);
    }

    public ArrayList<Employee> searchByNameFragment(String nameFragment){
        ArrayList<Employee> result= new ArrayList<>();
        for(Employee employee:staff){
            if(employee.myNameIsLike(nameFragment)){
                result.add(employee);
            }
        }
        return result;
    }

    public ArrayList<Employee> searchBySeniority(int yearsWorked){
        ArrayList<Employee> result= new ArrayList<>();
        for(Employee employee:staff){
            if(employee.hasEnoughExperience(yearsWorked)){
                result.add(employee);
            }
        }
        return result;
    }

    public void printStaffData(ArrayList<Employee> staff){
        for (Employee employee:staff){
            System.out.println(employee.getFullData());
        }
    }

    public void addEmployee(Employee employee){
        this.staff.add(employee);
    }

}