package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh12N063 {

    public static int[] averageStudentsPerClass(int[][] school){
        int[] result=new int[school.length];
        for(int i=0;i<school.length;i++){
            for(int j=0;j<school[i].length;j++){
                result[i]+=school[i][j];
            }
            result[i]/=school[i].length;
        }
        return result;
    }

}
