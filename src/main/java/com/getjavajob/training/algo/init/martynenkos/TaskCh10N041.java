package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N041 {

    public static long factorial(int n){
        return (n>1)? n*factorial(n-1):1;
    }

}
