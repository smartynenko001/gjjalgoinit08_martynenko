package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh09N107 {

    public static String swapCharacters(String word) {
        final char LETTER1 = 'a';
        final char LETTER2 = 'o';
        int storageIndexA = word.indexOf(LETTER1);
        int storageIndexO = word.lastIndexOf(LETTER2);
        StringBuilder result = new StringBuilder(word);

        if ((storageIndexA+1) * (storageIndexO+1) > 0) {
            result.deleteCharAt(storageIndexA);
            result.insert(storageIndexA, LETTER2);
            result.deleteCharAt(storageIndexO);
            result.insert(storageIndexO, LETTER1);
            return result.toString();
        } else {
            return "Swap is impossible";
        }
    }

}
