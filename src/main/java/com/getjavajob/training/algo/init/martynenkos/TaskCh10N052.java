package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N052 {

    public static void printInvertedNumber(int number){
        System.out.print(getInvertedNumber(number));
    }

    public static String getInvertedNumber(int number){
        if (number>9) {
            return Integer.toString(number%10).concat(getInvertedNumber(number/10));
        } else {
            return Integer.toString(number%10);
        }
    }

}
