package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh04N067 {

    private static final int JANUARY_1ST_DAY_OF_THE_WEEK_NUMBER=1;

    public static String isWeekendDay(int date) {
        return ((date-2+JANUARY_1ST_DAY_OF_THE_WEEK_NUMBER)%7>4) ? "Weekend" : "Workday";

    }

}
