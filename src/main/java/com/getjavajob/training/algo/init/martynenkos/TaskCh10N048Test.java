package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh10N048.maxArrayValue;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N048Test {

    public static void main(String args[]){
        testMaxArrayValue();
    }

    private static void testMaxArrayValue(){
         final double[] TEST_ARRAY=new double[]{100,2,3,400,5,6};
        assertEquals("Test maximum array value",400, maxArrayValue(TEST_ARRAY));
    }

}
