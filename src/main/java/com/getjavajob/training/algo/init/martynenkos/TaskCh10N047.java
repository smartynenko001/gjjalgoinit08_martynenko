package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N047 {

    public static int nthFibonacciTerm(int n){
        return (n>2)?nthFibonacciTerm(n-1)+nthFibonacciTerm(n-2):1;
    }

}
