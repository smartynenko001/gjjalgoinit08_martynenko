package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh02N013.reverseDigits;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh02N013Test {

    public static void main(String[] args){
        testReversion();
    }

    private static void testReversion(){
        final int TEST_NUMBER=128;
        final int RESULT_NUMBER=821;
        assertEquals("TaskCh02N013Test.reverseDigits",RESULT_NUMBER,reverseDigits(TEST_NUMBER));
    }

}
