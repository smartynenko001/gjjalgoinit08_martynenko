package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh01N017 {
    public static double partO(double x) {
        return Math.sqrt(1-Math.pow(Math.sin(x),2));
    }

    public static double partP(double a, double b, double c, double x) {
        return Math.pow(a*Math.pow(x,2)+b*x+c,-0.5);
    }

    public static double partR(double x) {
        return (Math.pow(x+1,0.5)+Math.pow(x-1,0.5))/2/Math.pow(x,0.5);
    }

    public static double partS(double x) {
        return Math.abs(x)+Math.abs(x+1);
    }

}
