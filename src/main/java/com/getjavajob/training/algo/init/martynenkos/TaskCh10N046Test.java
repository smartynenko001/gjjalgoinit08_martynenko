package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh10N046.nSumGeometric;
import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh10N046.nthTermGeometric;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N046Test {

    public static void main(String args[]){
        testNSumArithmetic();
        testNthMemberArithmetic();
    }

    private static void testNthMemberArithmetic(){
        assertEquals("Test nth Geometric Term Calculation",1024, nthTermGeometric(2,2,10));
    }

    private static void testNSumArithmetic(){
        assertEquals("Test nth Geometric Sum Calculation",2046,nSumGeometric(2,2,10));
    }

}
