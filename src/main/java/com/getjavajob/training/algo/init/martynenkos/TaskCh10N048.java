package main.java.com.getjavajob.training.algo.init.martynenkos;

import java.util.Arrays;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N048 {

    public static double maxArrayValue(double[] array){
          if(array.length>1){
            if(maxArrayValue(Arrays.copyOfRange(array,0,array.length-1))>array[array.length-1]){
                return maxArrayValue(Arrays.copyOfRange(array,0,array.length-1));
            }else{
              return array[array.length-1];
            }
        }else {
              return array[0];
          }
    }

}
