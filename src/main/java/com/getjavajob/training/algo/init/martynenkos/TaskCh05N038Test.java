package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh05N038.taskA;
import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh05N038.taskB;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh05N038Test {

    public static void main(String Args[]){
        testTaskA();
        testTaskB();
    }

    private static void testTaskA(){
        assertEquals("Test Task A, Final Distance from home",302,Math.round(taskA()*1000));
    }

    private static void testTaskB(){
        assertEquals("Test Task B, Distance travelled",4177,Math.round(taskB()*1000));
    }

}
