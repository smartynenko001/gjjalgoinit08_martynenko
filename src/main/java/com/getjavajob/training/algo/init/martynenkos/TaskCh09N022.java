package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh09N022 {

    public static String getFirstHalfOfTheWord(String word){
        return word.substring(0,(int) (word.length()*0.5));
    }

}
