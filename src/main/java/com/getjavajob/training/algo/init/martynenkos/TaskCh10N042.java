package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N042 {

    public static long power(long a,int n){
        return (n>1)? power(a,n-1)*a:a;
    }

}
