package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCH10N045 {

    public static double nthTermArithmetic(double firstTerm, double difference, int n){
        return (n>1)? nthTermArithmetic(firstTerm,difference,n-1)+difference:firstTerm;
    }

    public static double nSumArithmetic(double firstTerm,double difference,int n){
        return (n>1)? nSumArithmetic(firstTerm,difference,n-1)+ nthTermArithmetic(firstTerm,difference,n):firstTerm;
    }

}
