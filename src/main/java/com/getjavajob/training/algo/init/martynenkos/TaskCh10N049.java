package main.java.com.getjavajob.training.algo.init.martynenkos;

import java.util.Arrays;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh10N048.maxArrayValue;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N049 {

    public static int maxArrayIndex(double[] array){
        if(array.length>1){
            if(maxArrayValue(Arrays.copyOfRange(array,0,array.length-1))>array[array.length-1]){
                return maxArrayIndex(Arrays.copyOfRange(array,0,array.length-1));
            }else{
                return array.length-1;
            }
        }else {
            return 0;
        }
    }

}
