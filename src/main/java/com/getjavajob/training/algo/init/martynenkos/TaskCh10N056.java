package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N056 {

    private static boolean isPrime(int number,int divisor) {
        return number <= Math.pow(divisor, 2) || number % divisor != 0 && isPrime(number, divisor + 1);
    }

    public static boolean isPrime(int number){
        return isPrime(number,2);
    }

}
