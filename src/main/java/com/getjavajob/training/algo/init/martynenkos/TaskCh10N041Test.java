package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh10N041.factorial;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N041Test {

    public static void main(String args[]){
        testFactorial();
    }

    private static void testFactorial(){
        assertEquals("Test factorial calculation",40320,factorial(8));
    }

}
