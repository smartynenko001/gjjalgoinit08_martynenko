package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh10N049.maxArrayIndex;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N049Test {

    public static void main(String[] args){
        testMaxArrayIndex();
    }

    private static void testMaxArrayIndex(){
        final double[] TEST_ARRAY=new double[]{10,2,3,100,5};
        assertEquals("Test Index of Max element",3,maxArrayIndex(TEST_ARRAY));
    }

}
