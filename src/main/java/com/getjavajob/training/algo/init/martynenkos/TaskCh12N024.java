package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh12N024 {

    public static int[][] taskA(int n){
        int[][] result=new int[n][n];
        for(int i=0;i<n;i++){
            for (int j=0;j<n;j++){
                if((i==0||j==0)){
                    result[i][j]=1;
                }else{
                    result[i][j]=result[i][j-1]+result[i-1][j];
                }
            }
        }
        return result;
    }

    public static int[][] taskB(int n){
        int[][] result=new int[n][n];
        for(int i=0;i<n;i++){
            for (int j=0;j<n;j++){
                if((i==0)){
                    result[i][j]=j+1;
                }else{
                    result[i][j]=result[i-1][(j+1)%n];
                }
            }
        }
        return result;
    }

}
