package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh09N017.checkLettersEquality;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh09N017Test {

    public static void main(String args[]){
        testCheckLettersEquality();
        testCheckLettersInEquality();
    }

    private static void testCheckLettersEquality(){
        assertEquals("Test when first and last letter are equal",true,checkLettersEquality("asda"));
    }

    private static void testCheckLettersInEquality(){
        assertEquals("Test when first and last letter are not equal",false,checkLettersEquality("asdA"));
    }

}
