package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh03N026 {

    public static boolean taskA(boolean x, boolean y, boolean z){
        return !y&&!x;
    }

    public static boolean taskB(boolean x, boolean y,boolean z) {
        return x||!y;
    }

    public static boolean taskC(boolean x, boolean y, boolean z) {
        return x || !y && !(x || !z);
    }

}
