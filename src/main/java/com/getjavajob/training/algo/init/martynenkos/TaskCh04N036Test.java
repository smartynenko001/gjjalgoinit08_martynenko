package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh04N036.assertTrafficLightColor;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh04N036Test {

    public static void main(String agrs[]) {
        testRed();
        testGreen();
    }

    private static void testGreen() {
        assertEquals("Test1","Green",assertTrafficLightColor(5));
    }

    private static void testRed() {
        assertEquals("Test2","Red",assertTrafficLightColor(3));
    }

}
