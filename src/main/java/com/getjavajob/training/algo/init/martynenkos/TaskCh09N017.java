package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh09N017 {

    public static boolean checkLettersEquality(String word) {
         return word.charAt(word.length() - 1) == word.charAt(0);
        }

    }
