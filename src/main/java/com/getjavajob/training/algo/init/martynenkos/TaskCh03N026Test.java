package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh03N026.taskA;
import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh03N026.taskB;
import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh03N026.taskC;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh03N026Test {

    public static void main(String args[]) {
        testTaskA();
        testTaskB();
        testTaskC();
    }

    private static void testTaskA() {
        final boolean X=false;
        final boolean Y=false;
        final boolean Z=false;
        final boolean RESULT=true;
        assertEquals("TaskCh03N026Test.taskA",RESULT,taskA(X,Y,Z));
    }

    private static void  testTaskB() {
        final boolean X=true;
        final boolean Y=false;
        final boolean Z=false;
        final boolean RESULT=true;
        assertEquals("TaskCh03N026Test.taskB",RESULT,taskB(X,Y,Z));
    }

    private static void  testTaskC() {
        final boolean X=false;
        final boolean Y=false;
        final boolean Z=false;
        final boolean RESULT=false;
        assertEquals("TaskCh03N026Test.taskC",RESULT,taskC(X,Y,Z));
    }

}
