package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh09N185 {

    public static String checkBracesTaskA(String formula) {
        int result = 0;
        for (int i = 0; i <= (formula.length() - 1); i++) {
            switch(formula.charAt(i)){
                case '(':
                    result++;
                    break;
                case ')':
                    result--;
                    break;
                default:
                    break;
            }
            if (result < 0) {
                return "No";
            }
        }
        return (result==0)?"Yes":"No";
    }

    public static String checkBracesTaskB(String formula) {
        int result = 0;
        for (int i = 0; i <= (formula.length() - 1); i++) {
            switch(formula.charAt(i)){
                case '(':
                    result++;
                    break;
                case ')':
                    result--;
                    break;
                default:
                    break;
            }
            if (result < 0) {
                return "No, Extra right brace found at position "+Integer.toString(i+1);
            }
        }
        return (result==0)?"Yes":"No, "+Integer.toString(result)+" extra left brace(s) found";
    }

}
