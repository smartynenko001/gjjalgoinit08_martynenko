package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N043 {

  public static int getSumOfDigits(int number){
      return number>0 ? getSumOfDigits(number/10)+number%10 :0;
  }

    public static int countDigits(int number){
        return number>0 ? countDigits(number/10)+1 :0;
    }

}
