package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh06N008 {

    public static void printSquares(double number){
       for (int i=1;i<=Math.sqrt(number);i++){
           System.out.println(Math.pow(i,2));
       }
    }

}
