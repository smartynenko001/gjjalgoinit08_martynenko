package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh09N185.checkBracesTaskA;
import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh09N185.checkBracesTaskB;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh09N185Test {

    public static void main(String args[]){
        testCheckBracesTaskACorrect();
        testCheckBracesTaskAIncorrect();
        testCheckBracesTaskBCorrect();
        testCheckBracesTaskBIncorrectLeft();
        testCheckBracesTaskBIncorrectRight();
    }

    private static void testCheckBracesTaskACorrect(){
        assertEquals("Test Task A Correct Formula","Yes",checkBracesTaskA("(10+1)*8"));
    }

    private static void testCheckBracesTaskAIncorrect(){
        assertEquals("Test Task A Incorrect Formula","No",checkBracesTaskA("(10+1*8"));
    }

    private static void testCheckBracesTaskBCorrect(){
        assertEquals("Test Task A Correct Formula","Yes",checkBracesTaskB("(10+1)*8"));
    }

    private static void testCheckBracesTaskBIncorrectRight(){
        assertEquals("Test Task B Incorrect Formula With Right Brace"
                ,"No, Extra right brace found at position 1"
                ,checkBracesTaskB(")10+1*8"));
    }

    private static void testCheckBracesTaskBIncorrectLeft(){
        assertEquals("Test Task B Incorrect Formula With Left Brace"
                ,"No, 1 extra left brace(s) found"
                ,checkBracesTaskB("(10+1*8"));
    }

}
