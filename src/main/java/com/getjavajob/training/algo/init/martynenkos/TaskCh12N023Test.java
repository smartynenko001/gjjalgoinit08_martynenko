package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh12N023.taskA;
import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh12N023.taskB;
import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh12N023.taskC;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh12N023Test {

    public static void main(String args[]){
        testTaskA();
        testTaskB();
        testTaskC();
    }

    private static void testTaskA(){
        int[][] ARRAY_TASK_A=new int[][]{
                {1, 0, 0, 0, 0, 0, 1},
                {0, 1, 0, 0, 0, 1, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 0, 1, 0, 0},
                {0, 1, 0, 0, 0, 1, 0},
                {1, 0, 0, 0, 0, 0, 1}
        };
        assertEquals("Test Task A",ARRAY_TASK_A,taskA(ARRAY_TASK_A.length));
    }

    private static void testTaskB(){
        int[][] ARRAY_TASK_B={
                {1, 0, 0, 1, 0, 0, 1},
                {0, 1, 0, 1, 0, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {1, 1, 1, 1, 1, 1, 1},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 0, 1, 0, 1, 0},
                {1, 0, 0, 1, 0, 0, 1}
        };
        assertEquals("Test Task B",ARRAY_TASK_B,taskB(ARRAY_TASK_B.length));
    }

    private static void testTaskC(){
        int[][] ARRAY_TASK_C={
                {1, 1, 1, 1, 1, 1, 1},
                {0, 1, 1, 1, 1, 1, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 0, 0, 1, 0, 0, 0},
                {0, 0, 1, 1, 1, 0, 0},
                {0, 1, 1, 1, 1, 1, 0},
                {1, 1, 1, 1, 1, 1, 1}
        };
        assertEquals("Test Task C",ARRAY_TASK_C,taskC(ARRAY_TASK_C.length));
    }

}
