package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh04N106.assertSeasonByMonthNumber;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh04N106Test {

    public static void main(String args[]){
        testWinter();
        testSpring();
        testSummer();
        testAutumn();
    }

    private static void testWinter() {
        assertEquals("Test Winter Months","Winter",assertSeasonByMonthNumber(12));
    }

    private static void testSpring() {
        assertEquals("Test Spring Months","Spring",assertSeasonByMonthNumber(4));
    }

    private static void testSummer() {
        assertEquals("Test Summer Months","Summer",assertSeasonByMonthNumber(8));
    }

    private static void testAutumn() {
        assertEquals("Test Autumn Months","Autumn",assertSeasonByMonthNumber(9));
    }

}
