package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh11N245.shuffleArray;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh11N245Test {

    public static void main(String args[]){
        testShuffleArray();
    }

    private static void testShuffleArray(){
        final int[] INITIAL_ARRAY= {1,-2,5,-8,7,10,0};
        final int[] RESULT_ARRAY= {-2,-8,0,1,5,7,10};
        assertEquals("Test TaskCh11N245",RESULT_ARRAY,shuffleArray(INITIAL_ARRAY));
    }

}
