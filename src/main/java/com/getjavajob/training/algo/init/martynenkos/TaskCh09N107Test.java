package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh09N107.swapCharacters;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh09N107Test {

    public static void main(String args[]){
        testSwapCharactersExist();
        testSwapCharactersNotExist();
    }

    private static void testSwapCharactersExist(){
      assertEquals("Test correctness of swapped characters when both exist:","xaxox",swapCharacters("xoxax"));
    }

    private static void testSwapCharactersNotExist(){
        assertEquals(
                "Test correctness of swapped characters when at least one does not exist:"
                ,"Swap is impossible"
                ,swapCharacters("x")
        );
    }

}
