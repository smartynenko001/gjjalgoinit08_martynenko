package main.java.com.getjavajob.training.algo.init.martynenkos;

import java.util.Arrays;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh12N025 {

    public static void printAllArrays(){
        final int NUMBER_OF_ROWS=3;
        final int NUMBER_OF_COLUMNS=4;
        System.out.println(Arrays.deepToString(task1(NUMBER_OF_ROWS,NUMBER_OF_COLUMNS)));
        System.out.println(Arrays.deepToString(task2(NUMBER_OF_ROWS,NUMBER_OF_COLUMNS)));
        System.out.println(Arrays.deepToString(task3(NUMBER_OF_ROWS,NUMBER_OF_COLUMNS)));
        System.out.println(Arrays.deepToString(task4(NUMBER_OF_ROWS,NUMBER_OF_COLUMNS)));
        System.out.println(Arrays.deepToString(task5(NUMBER_OF_ROWS,NUMBER_OF_COLUMNS)));
        System.out.println(Arrays.deepToString(task6(NUMBER_OF_ROWS,NUMBER_OF_COLUMNS)));
        System.out.println(Arrays.deepToString(task7(NUMBER_OF_ROWS,NUMBER_OF_COLUMNS)));
        System.out.println(Arrays.deepToString(task8(NUMBER_OF_ROWS,NUMBER_OF_COLUMNS)));
        System.out.println(Arrays.deepToString(task9(NUMBER_OF_ROWS,NUMBER_OF_COLUMNS)));
        System.out.println(Arrays.deepToString(task10(NUMBER_OF_ROWS,NUMBER_OF_COLUMNS)));
        System.out.println(Arrays.deepToString(task11(NUMBER_OF_ROWS,NUMBER_OF_COLUMNS)));
        System.out.println(Arrays.deepToString(task12(NUMBER_OF_ROWS,NUMBER_OF_COLUMNS)));
        System.out.println(Arrays.deepToString(task13(NUMBER_OF_ROWS,NUMBER_OF_COLUMNS)));
        System.out.println(Arrays.deepToString(task14(NUMBER_OF_ROWS,NUMBER_OF_COLUMNS)));
        System.out.println(Arrays.deepToString(task15(NUMBER_OF_ROWS,NUMBER_OF_COLUMNS)));
        System.out.println(Arrays.deepToString(task16(NUMBER_OF_ROWS,NUMBER_OF_COLUMNS)));
    }

    private static int[][] task1(int m, int n){
        int[][] result=new int[m][n];
        for(int i=0;i<n*m;i++){
            result[i/n][i%n]=i+1;
        }
        return result;
    }

    private static int[][] task2(int m, int n){
        int[][] result=new int[m][n];
        for(int i=0;i<n*m;i++){
            result[i%m][i/m]=i+1;
        }
        return result;
    }

    private static int[][] task3(int m, int n){
        int[][] result=new int[m][n];
        for(int i=0;i<n*m;i++){
            result[i/n][n-1-i%n]=i+1;
        }
        return result;
    }

    private static int[][] task4(int m, int n){
        int[][] result=new int[m][n];
        for(int i=0;i<n*m;i++){
            result[m-1-i%m][i/m]=i+1;
        }
        return result;
    }

    private static int[][] task5(int m, int n){
        int[][] result=new int[m][n];
        for(int i=0;i<n*m;i++){
            result[i/n][((i/n)%2==0)?i%n:n-1-i%n]=i+1;
        }
        return result;
    }

    private static int[][] task6(int m, int n){
        int[][] result=new int[m][n];
        for(int i=0;i<n*m;i++){
            result[((i/m)%2==0)?i%m:m-1-i%m][i/m]=i+1;
        }
        return result;
    }

    private static int[][] task7(int m, int n){
        int[][] result=new int[m][n];
        for(int i=0;i<n*m;i++){
            result[m-1-i%m][i/m]=i+1;
        }
        return result;
    }

    private static int[][] task8(int m, int n){
        int[][] result=new int[m][n];
        for(int i=0;i<n*m;i++){
            result[i%m][n-1-i/m]=i+1;
        }
        return result;
    }

    private static int[][] task9(int m, int n){
        int[][] result=new int[m][n];
        for(int i=0;i<n*m;i++){
            result[m-1-i/n][n-1-i%n]=i+1;
        }
        return result;
    }

    private static int[][] task10(int m, int n){
        int[][] result=new int[m][n];
        for(int i=0;i<n*m;i++){
            result[m-1-i%m][n-1-i/m]=i+1;
        }
        return result;
    }

    private static int[][] task11(int m, int n){
        int[][] result=new int[m][n];
        for(int i=0;i<n*m;i++){
            result[m-1-i/n][((i/n)%2==0)?i%n:n-1-i%n]=i+1;
        }
        return result;
    }

    private static int[][] task12(int m, int n){
        int[][] result=new int[m][n];
        for(int i=0;i<n*m;i++){
            result[i/n][((i/n)%2==0)?n-1-i%n:i%n]=i+1;
        }
        return result;
    }

    private static int[][] task13(int m, int n){
        int[][] result=new int[m][n];
        for(int i=0;i<n*m;i++){
            result[((i/m)%2==0)?i%m:m-1-i%m][n-1-i/m]=i+1;
        }
        return result;
    }

    private static int[][] task14(int m, int n){
        int[][] result=new int[m][n];
        for(int i=0;i<n*m;i++){
            result[((i/m)%2==0)?m-1-i%m:i%m][i/m]=i+1;
        }
        return result;
    }

    private static int[][] task15(int m, int n){
        int[][] result=new int[m][n];
        for(int i=0;i<n*m;i++){
            result[m-1-i/n][((i/n)%2==0)?n-1-i%n:i%n]=i+1;
        }
        return result;
    }

    private static int[][] task16(int m, int n){
        int[][] result=new int[m][n];
        for(int i=0;i<n*m;i++){
            result[((i/m)%2==0)?m-1-i%m:i%m][n-1-i/m]=i+1;
        }
        return result;
    }

}
