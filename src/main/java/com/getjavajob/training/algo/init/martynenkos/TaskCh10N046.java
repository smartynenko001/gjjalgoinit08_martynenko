package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N046 {

    public static double nthTermGeometric(double firstTerm,double ratio,int n){
        return (n>1)? nthTermGeometric(firstTerm,ratio,n-1)*ratio:firstTerm;
    }

    public static double nSumGeometric(double firstTerm,double ratio,int n){
        return (n>1)? nSumGeometric(firstTerm,ratio,n-1)+nthTermGeometric(firstTerm,ratio,n):firstTerm;
    }

}
