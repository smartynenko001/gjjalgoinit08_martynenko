package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh09N042.invertWord;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh09N042Test {

    public static void main(String args[]){
        testInvertWord();
    }

    private static void testInvertWord(){
        final String TEST_WORD="dcba";
        final String EXPECTED_RESULT="abcd";
        assertEquals("Test if the string reverted correctly",EXPECTED_RESULT,invertWord(TEST_WORD));
    }

}
