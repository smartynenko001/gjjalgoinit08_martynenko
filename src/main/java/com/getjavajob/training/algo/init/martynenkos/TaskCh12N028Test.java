package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh12N028.spiralArrayFill;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh12N028Test {

    public static void main(String args[]){
        testSpiralArrayFill();
    }

    private static void testSpiralArrayFill(){
        int[][] ARRAY_TEST={
                {1, 2, 3, 4, 5},
                {16, 17, 18, 19, 6},
                {15, 24, 25, 20, 7},
                {14, 23, 22, 21, 8},
                {13, 12, 11, 10, 9}
        };
        assertEquals("Test Spiral Fill",ARRAY_TEST,spiralArrayFill(ARRAY_TEST.length));
    }

}
