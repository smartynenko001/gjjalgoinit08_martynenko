package main.java.com.getjavajob.training.algo.init.martynenkos;

import java.util.ArrayList;

import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh13N012Test {

    public static void main(String args[]){
        testSearchBySeniority();
        testSearchByNameFragment();
    }

    private static void testSearchBySeniority(){
        Database myCompany=new Database();
        final String NAME1="Ivan";
        final String NAME2="Semen";
        final String NAME3="Jose";
        final String NAME4="Feng";
        final String PATRONYMIC1="Ivanovich";
        final String PATRONYMIC2="Ivanovich";
        final String PATRONYMIC3="Maria";
        final String LAST_NAME1="Ivanov";
        final String LAST_NAME2="Ivanov";
        final String LAST_NAME3="Rodriguez";
        final String LAST_NAME4="Lee";
        final String CITY1="Moscow";
        final String CITY2="Naryan-Mar";
        final String CITY3="Santa Clara";
        final String CITY4="Makao";
        final int YEAR1=2000;
        final int YEAR2=2016;
        final int YEAR3=2005;
        final int YEAR4=2012;
        final int MONTH1=3;
        final int MONTH2=3;
        final int MONTH3=12;
        final int MONTH4=1;
        final int YEARS_WORKED=3;
        Employee employee1=new Employee(NAME1,PATRONYMIC1,LAST_NAME1,CITY1,YEAR1,MONTH1);
        Employee employee2=new Employee(NAME2,PATRONYMIC2,LAST_NAME2,CITY2,YEAR2,MONTH2);
        Employee employee3=new Employee(NAME3,PATRONYMIC3,LAST_NAME3,CITY3,YEAR3,MONTH3);
        Employee employee4=new Employee(NAME4,LAST_NAME4,CITY4,YEAR4,MONTH4);
        myCompany.addEmployee(employee1);
        myCompany.addEmployee(employee2);
        myCompany.addEmployee(employee3);
        myCompany.addEmployee(employee4);
        ArrayList<Employee> controlList= new ArrayList<>();
        controlList.add(employee1);
        controlList.add(employee3);
        controlList.add(employee4);
        assertEquals("Test Database Search by Work Experience",controlList,myCompany.searchBySeniority(YEARS_WORKED));
    }

    private static void testSearchByNameFragment(){
        Database myCompany=new Database();
        final String NAME1="Ivan";
        final String NAME2="Semen";
        final String NAME3="Jose";
        final String NAME4="Feng";
        final String PATRONYMIC1="Ivanovich";
        final String PATRONYMIC2="Ivanovich";
        final String PATRONYMIC3="Maria";
        final String LAST_NAME1="Ivanov";
        final String LAST_NAME2="Ivanov";
        final String LAST_NAME3="Rodriguez";
        final String LAST_NAME4="Lee";
        final String CITY1="Moscow";
        final String CITY2="Naryan-Mar";
        final String CITY3="Santa Clara";
        final String CITY4="Makao";
        final int YEAR1=2000;
        final int YEAR2=2016;
        final int YEAR3=2005;
        final int YEAR4=2012;
        final int MONTH1=3;
        final int MONTH2=3;
        final int MONTH3=12;
        final int MONTH4=1;
        final String FRAGMENT_TO_FIND="IvAnO";
        Employee employee1=new Employee(NAME1,PATRONYMIC1,LAST_NAME1,CITY1,YEAR1,MONTH1);
        Employee employee2=new Employee(NAME2,PATRONYMIC2,LAST_NAME2,CITY2,YEAR2,MONTH2);
        Employee employee3=new Employee(NAME3,PATRONYMIC3,LAST_NAME3,CITY3,YEAR3,MONTH3);
        Employee employee4=new Employee(NAME4,LAST_NAME4,CITY4,YEAR4,MONTH4);
        myCompany.addEmployee(employee1);
        myCompany.addEmployee(employee2);
        myCompany.addEmployee(employee3);
        myCompany.addEmployee(employee4);
        ArrayList<Employee> controlList= new ArrayList<>();
        controlList.add(employee1);
        controlList.add(employee2);
        assertEquals("Test Database Search by Name Engine",controlList,myCompany.searchByNameFragment(FRAGMENT_TO_FIND));
    }

}
