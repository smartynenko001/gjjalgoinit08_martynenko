package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh10N047.nthFibonacciTerm;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N047Test {

    public static void main(String args[]){
        testNthFibonacciTerm();
    }

    private static void testNthFibonacciTerm(){
        assertEquals("Test nth Fibonacci Term",8,nthFibonacciTerm(6));
    }

}
