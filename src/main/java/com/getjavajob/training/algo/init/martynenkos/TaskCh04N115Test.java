package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh04N115.TaskA;
import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh04N115.TaskB;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh04N115Test {

    public static void main(String args[]) {
        testGreen();
        testRed();
        testYellow();
        testWhite();
        testBlack();

        testRat();
        testBull();
        testTiger();
        testHare();
        testDragon();
        testSnake();
        testHorse();
        testSheep();
        testMonkey();
        testRooster();
        testDog();
        testPig();

        testTaskB();
    }

    private static void testGreen() {
        final int TEST_YEAR=2024;
        final String EXPECTED_RESULT="Dragon, Green";
        assertEquals("Test Green",EXPECTED_RESULT,TaskA(TEST_YEAR));
    }

    private static void testRed() {
        final int TEST_YEAR=2036;
        final String EXPECTED_RESULT="Dragon, Red";
        assertEquals("Test Red",EXPECTED_RESULT,TaskA(TEST_YEAR));
    }

    private static void testYellow() {
        final int TEST_YEAR=2048;
        final String EXPECTED_RESULT="Dragon, Yellow";
        assertEquals("Test Yellow",EXPECTED_RESULT,TaskA(TEST_YEAR));
    }

    private static void testWhite() {
        final int TEST_YEAR=2060;
        final String EXPECTED_RESULT="Dragon, White";
        assertEquals("Test White",EXPECTED_RESULT,TaskA(TEST_YEAR));
    }

    private static void testBlack() {
        final int TEST_YEAR=2072;
        final String EXPECTED_RESULT="Dragon, Black";
        assertEquals("Test Black",EXPECTED_RESULT,TaskA(TEST_YEAR));
    }

    private static void testRat() {
        final int TEST_YEAR=1984;
        final String EXPECTED_RESULT="Rat, Green";
        assertEquals("Test Rat",EXPECTED_RESULT,TaskA(TEST_YEAR));
    }

    private static void testBull() {
        final int TEST_YEAR=1985;
        final String EXPECTED_RESULT="Bull, Green";
        assertEquals("Test Bull",EXPECTED_RESULT,TaskA(TEST_YEAR));
    }

    private static void testTiger() {
        final int TEST_YEAR=1986;
        final String EXPECTED_RESULT="Tiger, Red";
        assertEquals("Test Tiger",EXPECTED_RESULT,TaskA(TEST_YEAR));
    }

    private static void testHare() {
        final int TEST_YEAR=1987;
        final String EXPECTED_RESULT="Hare, Red";
        assertEquals("Test Hare",EXPECTED_RESULT,TaskA(TEST_YEAR));
    }

    private static void testDragon() {
        final int TEST_YEAR=1988;
        final String EXPECTED_RESULT="Dragon, Yellow";
        assertEquals("Test Dragon",EXPECTED_RESULT,TaskA(TEST_YEAR));
    }

    private static void testSnake() {
        final int TEST_YEAR=1989;
        final String EXPECTED_RESULT="Snake, Yellow";
        assertEquals("Test Snake",EXPECTED_RESULT,TaskA(TEST_YEAR));
    }

    private static void testHorse() {
        final int TEST_YEAR=1990;
        final String EXPECTED_RESULT="Horse, White";
        assertEquals("Test Horse",EXPECTED_RESULT,TaskA(TEST_YEAR));
    }

    private static void testSheep() {
        final int TEST_YEAR=1991;
        final String EXPECTED_RESULT="Sheep, White";
        assertEquals("Test Sheep",EXPECTED_RESULT,TaskA(TEST_YEAR));
    }

    private static void testMonkey() {
        final int TEST_YEAR=1992;
        final String EXPECTED_RESULT="Monkey, Black";
        assertEquals("Test Monkey",EXPECTED_RESULT,TaskA(TEST_YEAR));
    }

    private static void testRooster() {
        final int TEST_YEAR=1993;
        final String EXPECTED_RESULT="Rooster, Black";
        assertEquals("Test Rooster",EXPECTED_RESULT,TaskA(TEST_YEAR));
    }

    private static void testDog() {
        final int TEST_YEAR=1994;
        final String EXPECTED_RESULT="Dog, Green";
        assertEquals("Test Dog",EXPECTED_RESULT,TaskA(TEST_YEAR));
    }

    private static void testPig() {
        final int TEST_YEAR=1995;
        final String EXPECTED_RESULT="Pig, Green";
        assertEquals("Test Pig",EXPECTED_RESULT,TaskA(TEST_YEAR));
    }

    private static void testTaskB() {
        final int TEST_YEAR=1950;
        final String EXPECTED_RESULT="Tiger, White";
        assertEquals("Test Task B",EXPECTED_RESULT,TaskB(TEST_YEAR));
    }

}
