package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh09N15.returnCharacter;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh09N15Test {

    public static void main(String args[]){
        testReturnCharacter();
    }

    private static void testReturnCharacter(){
        final String TEST_WORD="qwerty";
        final int CHARACTER_POSITION=2;
        final char EXPECTED_RESULT='w';
        assertEquals("Test ReturnCharacter",EXPECTED_RESULT,returnCharacter(TEST_WORD,CHARACTER_POSITION));
    }

}
