package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh09N166 {

    public static StringBuffer swapWords(String sentence){
        String[] wordsInSentence=sentence.split(" ");
        StringBuffer result1=new StringBuffer();
        result1.append(wordsInSentence[wordsInSentence.length-1]);
        for (int i=1;i<=wordsInSentence.length-2;i++){
           result1=result1.append(" ").append(wordsInSentence[i]);
        }
        return result1.append(" ").append(wordsInSentence[0]);
    }

}
