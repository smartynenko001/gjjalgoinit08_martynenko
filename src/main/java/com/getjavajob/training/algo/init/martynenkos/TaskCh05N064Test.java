package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh05N064.calculatePopulationDensity;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh05N064Test {

    public static void main(String args[]){
        testCalculatePopulationDensity();
    }

    private static void testCalculatePopulationDensity(){
        final double[] population={1,2,3,4,5,6,7,8,9,10,11,12};
        final double[] area={1,2,3,4,5,6,7,8,9,10,11,12};
        assertEquals("Test calculatePopulationDensity",1000,calculatePopulationDensity(population,area));

    }

}
