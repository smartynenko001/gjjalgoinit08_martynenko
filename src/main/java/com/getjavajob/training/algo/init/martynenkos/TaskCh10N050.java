package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N050 {

    public static int Akkerman(int n,int m){
        if (n==0){
            return m+1;
        }
        if (m==0){
            return Akkerman(n-1,1);
        }
        return Akkerman(n-1,Akkerman(n,m-1));
    }

}
