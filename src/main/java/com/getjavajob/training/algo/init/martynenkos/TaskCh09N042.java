package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh09N042 {

    public static String invertWord(String word){
        StringBuilder result=new StringBuilder(word);
        return result.reverse().toString();
    }

}
