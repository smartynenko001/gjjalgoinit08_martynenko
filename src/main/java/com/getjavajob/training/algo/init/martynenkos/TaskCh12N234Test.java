package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh12N234.deleteRow;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh12N234Test {

    public static void main(String args[]){
        testDeleteRow();
    }

    private static void testDeleteRow(){
        int[][] TEST_ARRAY={
                {1,2,3},
                {4,5,6},
                {7,8,9}
        };
        int[][] RESULT_ARRAY={
                {1,2,3},
                {7,8,9},
                {0,0,0}
        };
        final int ROW_NUMBER=2;
        assertEquals("Test delition of a row",RESULT_ARRAY,deleteRow(TEST_ARRAY,ROW_NUMBER));
    }

}
