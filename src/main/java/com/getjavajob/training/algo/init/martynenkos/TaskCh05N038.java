package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh05N038 {

    private static final int N=100;

    public static double taskA(){
        double finalDistance=0;
        double[] intervalsTravelled=calculateIntervals();
        for (int i=2;i<N;i++) {
            finalDistance+=intervalsTravelled[i];
        }
        return finalDistance;
    }

    public static double taskB(){
        double finalDistance=0;
        double[] intervalsTravelled=calculateIntervals();
        for (int i=2;i<N;i++) {
            finalDistance+=Math.abs(intervalsTravelled[i]);
        }
        return finalDistance;
    }

    private static double[] calculateIntervals(){
        double[] temporaryArray=new double[N];
        for (int i=2;i<N;i++) {
            temporaryArray[i]=1/(double)i*(2*((i+1)%2)-1);
        }
        return temporaryArray;
    }

}
