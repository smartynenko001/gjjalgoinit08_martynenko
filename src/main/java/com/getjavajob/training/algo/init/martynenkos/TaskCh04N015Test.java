package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh04N015.calculateAge;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh04N015Test {

    public static void main(String args[]){
        testFullYearHasNotPassed();
        testFullYearHasPassed();
        testNowIsABirthdayMonth();
    }

    private static void testFullYearHasPassed() {
        final int YEAR_OF_BIRTH=1985;
        final int MONTH_OF_BIRTH=6;
        final int CURRENT_YEAR=2014;
        final int CURRENT_MONTH=12;
        final int EXPECTED_AGE=29;
        assertEquals("TaskCh04N015Test.Test1",EXPECTED_AGE, calculateAge(YEAR_OF_BIRTH,MONTH_OF_BIRTH,CURRENT_YEAR,CURRENT_MONTH));
    }

    private static  void testFullYearHasNotPassed() {
        final int YEAR_OF_BIRTH=1985;
        final int MONTH_OF_BIRTH=6;
        final int CURRENT_YEAR=2014;
        final int CURRENT_MONTH=5;
        final int EXPECTED_AGE=28;
        assertEquals("TaskCh04N015Test.Test2",EXPECTED_AGE, calculateAge(YEAR_OF_BIRTH,MONTH_OF_BIRTH,CURRENT_YEAR,CURRENT_MONTH));
    }

    private static void testNowIsABirthdayMonth() {
        final int YEAR_OF_BIRTH=1985;
        final int MONTH_OF_BIRTH=6;
        final int CURRENT_YEAR=2014;
        final int CURRENT_MONTH=6;
        final int EXPECTED_AGE=29;
        assertEquals("TaskCh04N015Test.Test3",EXPECTED_AGE, calculateAge(YEAR_OF_BIRTH,MONTH_OF_BIRTH,CURRENT_YEAR,CURRENT_MONTH));
    }

}
