package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh10N053.reverseArray;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N053Test {

    public static void main(String args[]) {
        testReverseArray();
    }

    private static void testReverseArray(){
        final int[] ARRAY_TEST={1,2,3,4,5,6,7,8,9};
        final int[] ARRAY_RESULT={9,8,7,6,5,4,3,2,1};
        assertEquals("Test array reversion:",ARRAY_RESULT,reverseArray(ARRAY_TEST));
    }

}
