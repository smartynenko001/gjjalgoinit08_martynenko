package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N051 {

    public static void TaskA(int n) {
        if (n > 0) {
            System.out.println(n);
            TaskA(n - 1);
        }
        }

    public static void TaskB(int n) {
        if (n > 0) {
            TaskB(n - 1);
            System.out.println(n);
        }
    }

    public static void TaskC(int n) {
        if (n > 0) {
            System.out.println(n);
            TaskC(n - 1);
            System.out.println(n);
        }
    }

}
