package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh04N115 {

    public static String TaskA(int year) {
     return getChineseYearAnimal(year)+", "+getChineseYearColor(year);
    }

    public static String TaskB(int year) {
      return TaskA(year+6000);
    }

    private static String getChineseYearColor(int year) {
        switch ((year - 1984) % 10) {
            case 0:
            case 1:
                return "Green";
            case 2:
            case 3:
                return "Red";
            case 4:
            case 5:
                return "Yellow";
            case 6:
            case 7:
                return "White";
            default:
                return "Black";
        }
    }

    private static String getChineseYearAnimal(int year) {
        switch ((year - 1984) % 12) {
            case 0:
                return "Rat";
            case 1:
                return "Bull";
            case 2:
                return "Tiger";
            case 3:
                return "Hare";
            case 4:
                return "Dragon";
            case 5:
                return "Snake";
            case 6:
                return "Horse";
            case 7:
                return "Sheep";
            case 8:
                return "Monkey";
            case 9:
                return "Rooster";
            case 10:
            case -2:
                return "Dog";
            default:
                return "Pig";
        }
    }
}
