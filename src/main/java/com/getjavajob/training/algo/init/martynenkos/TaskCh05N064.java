package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh05N064 {

    public static final int N=12;

    public static double calculatePopulationDensity(double[] population,double[] area){
        double totalPopulation=0;
        double totalArea=0;

        for (int i=1;i<=population.length-1;i++){
            totalPopulation+=population[i];
            totalArea+=area[i];
        }
        return totalPopulation/totalArea*1000;
    }

}

