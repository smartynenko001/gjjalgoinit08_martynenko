package main.java.com.getjavajob.training.algo.init.martynenkos;

import java.util.Arrays;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh12N028 {

    public static void main(String args[]){
        System.out.println(Arrays.deepToString(spiralArrayFill(5)));
    }

    public static int[][] spiralArrayFill(int n){
        int[][] result=new int[n][n];
        int i=1;
        for(int spiralNumber=1;spiralNumber<=n/2;spiralNumber++)
        {
            for (int rowNumber=spiralNumber-1;rowNumber<n-spiralNumber+1;rowNumber++) {
                result[spiralNumber-1][rowNumber]=i++;
            }
            for (int columnNumber=spiralNumber;columnNumber<n-spiralNumber+1;columnNumber++){
                result[columnNumber][n-spiralNumber]=i++;
            }
            for (int rowNumber=n-spiralNumber-1;rowNumber>=spiralNumber-1;--rowNumber){
                result[n-spiralNumber][rowNumber]=i++;
            }
            for (int columnNumber=n-spiralNumber-1;columnNumber>=spiralNumber;columnNumber--){
                result[columnNumber][spiralNumber-1]=i++;
            }
        }
        result[n/2][n/2]=n*n;
        return result;
    }

}
