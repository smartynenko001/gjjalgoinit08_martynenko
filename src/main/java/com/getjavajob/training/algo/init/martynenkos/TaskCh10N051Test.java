package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh10N051.TaskA;
import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh10N051.TaskB;
import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh10N051.TaskC;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N051Test {

    public static void main(String args[]){
        TaskA(5);
        System.out.println();
        TaskB(5);
        System.out.println();
        TaskC(5);
    }

}
