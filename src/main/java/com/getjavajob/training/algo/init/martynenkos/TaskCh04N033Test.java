package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh04N033.isEven;
import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh04N033.isOdd;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh04N033Test {

    public static void main(String agrs[]) {
        testTaskAEven();
        testTaskAOdd();
        testTaskBEven();
        testTaskBOdd();
    }

    private static void testTaskAEven(){
        assertEquals("Task A, Test even Numbers:",true,isEven(10));
    }

    private static void testTaskAOdd(){
        assertEquals("Task A, Test odd Numbers:",false,isEven((11)));
    }

    private static void testTaskBEven(){
        assertEquals("Task B, Test even Numbers:",false,isOdd(10));
    }

    private static void testTaskBOdd(){
        assertEquals("Task B, Test odd Numbers:",true,isOdd((11)));
    }

}
