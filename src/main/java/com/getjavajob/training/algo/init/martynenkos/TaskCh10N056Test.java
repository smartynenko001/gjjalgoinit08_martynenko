package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh10N056.isPrime;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N056Test {

    public static void main(String args[]){
        testIsPrime();
        testIsNotPrime();
    }

    private static void testIsPrime(){
        assertEquals("Test Prime Number Indicator",true,isPrime(29));
    }

    private static void testIsNotPrime(){
        assertEquals("Test Not Prime Number Indicator",false,isPrime(28));
    }

}
