package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh12N024.taskA;
import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh12N024.taskB;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh12N024Test {

    public static void main(String args[]){
        testTaskA();
        testTaskB();
    }

    private static void testTaskA(){
        int[][] ARRAY_TASK_A=new int[][]{
                {1, 1, 1, 1, 1, 1},
                {1, 2, 3, 4, 5, 6},
                {1, 3, 6, 10, 15, 21},
                {1, 4, 10, 20, 35, 56},
                {1, 5, 15, 35, 70, 126},
                {1, 6, 21, 56, 126, 252}
        };
        assertEquals("Test Task A",ARRAY_TASK_A,taskA(ARRAY_TASK_A.length));
    }

    private static void testTaskB(){
        int[][] ARRAY_TASK_B={
                {1, 2, 3, 4, 5, 6},
                {2, 3, 4, 5, 6, 1},
                {3, 4, 5, 6, 1, 2},
                {4, 5, 6, 1, 2, 3},
                {5, 6, 1, 2, 3, 4},
                {6, 1, 2, 3, 4, 5},
        };
        assertEquals("Test Task B",ARRAY_TASK_B,taskB(ARRAY_TASK_B.length));
    }

}
