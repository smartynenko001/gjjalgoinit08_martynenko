package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh11N245 {

    public static  int[] shuffleArray(int[] array){
        int[] nonPositiveElements=new int[array.length];
        int[] positiveElements=new int[array.length];
        int[] result=new int[array.length];
        int counterNonPositives=0;
        int counterPositives=0;
        for (int anArray : array) {
            if (anArray <= 0) {
                nonPositiveElements[counterNonPositives] = anArray;
                counterNonPositives++;
            } else {
                positiveElements[counterPositives] = anArray;
                counterPositives++;
            }
        }
        System.arraycopy(nonPositiveElements,0,result,0,counterNonPositives);
        System.arraycopy(positiveElements,0,result,counterNonPositives,counterPositives);
        return result;
    }

}
