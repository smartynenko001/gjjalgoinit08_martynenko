package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh02N043.isPossibleToDivide;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh02N043Test {

    public static void main(String args[]){
        testIsPossibleToDivide();
    }

    private static void testIsPossibleToDivide(){
        final int NUMBER1=85;
        final int NUMBER2=5;
        final int EXPECTED_RESULT=1;
        assertEquals("TaskCh02N043Test.isPossibleToDivide",EXPECTED_RESULT,isPossibleToDivide(NUMBER1,NUMBER2));
    }

}
