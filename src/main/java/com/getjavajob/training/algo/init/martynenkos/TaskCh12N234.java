package main.java.com.getjavajob.training.algo.init.martynenkos;

import java.util.Arrays;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh12N234 {

    public static int[][] deleteRow(int[][] array,int k){
        System.arraycopy(array,k,array,k-1,array.length-k);
        Arrays.fill(array[array.length-1],5);
        return array;
    }

}
