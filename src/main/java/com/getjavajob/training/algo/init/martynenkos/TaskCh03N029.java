package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * TaskA tests if both integers are even
 * TaskB tests if and only if one number is smaller than 20
 * TaskV tests if any number is 0
 * TaskG tests if all 3 numbers are negative
 * TaskD tests if and only if one number is divisible by 5
 * TaskE test if at least one number is greater than 100
 */
class TaskCh03N029 {

    public static boolean taskA(int x,int y) {
        return x%2==0 && y%2==0;
    }

    public static boolean taskB(double x, double y) {
        return  x<20 ^ y<20;
    }

    public static boolean taskV(double x, double y) {
        return x==0 || y==0;
    }

    public static boolean taskG(double x, double y, double z) {
        return x<0 && y<0 && z<0;
    }

    public static boolean taskD(double x, double y, double z) {
        return x%5==0 ^ y%5==0 ^ z%5==0;
    }

    public static boolean taskE(double x, double y, double z) {
        return x>100 || y>100 || z>100;
    }

}
