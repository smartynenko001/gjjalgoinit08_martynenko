package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh02N013 {

    public static int reverseDigits (int n) {
        return n+(n % 10)*99-99;
    }

}
