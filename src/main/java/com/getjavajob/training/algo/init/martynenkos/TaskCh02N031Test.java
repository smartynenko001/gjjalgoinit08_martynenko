package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh02N031.flipNumber;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh02N031Test {

    public static void main(String[] args) {
        testFlipNumber();
    }

    private static void testFlipNumber() {
        final int TEST_NUMBER=713;
        final int RESULT_NUMBER=731;
        assertEquals("TaskCh02N031Test.flipNumber",RESULT_NUMBER, flipNumber(TEST_NUMBER));
    }

}
