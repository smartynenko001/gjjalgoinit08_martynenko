package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh10N043.countDigits;
import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh10N043.getSumOfDigits;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N043Test {

    public static void main(String args[]){
        testCountDigits();
        testGetSumOfDigits();
    }

    private static void testGetSumOfDigits(){
        assertEquals("Test sum of digits calculation",8,getSumOfDigits (35));
    }

    private static void testCountDigits(){
        assertEquals("Test sum of digits calculation",2,countDigits(35));
    }

}
