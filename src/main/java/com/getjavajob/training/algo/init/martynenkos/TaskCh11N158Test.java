package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh11N158.removeDuplicates;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh11N158Test {

    public static void main(String args[]){
        testRemoveDuplicates();
    }

    private static void testRemoveDuplicates(){
        int[] testArray= new int[] {1,1,2,5,0,1,7,2,0,5};
        final int[] RESULT_ARRAY= new int[] {1,2,5,0,7,0,0,0,0,0};
        removeDuplicates(testArray);
        assertEquals("Test Remove Duplicates",RESULT_ARRAY,testArray);
    }

}
