package main.java.com.getjavajob.training.algo.init.martynenkos;

import java.util.Arrays;

import static java.util.Arrays.copyOfRange;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N053 {

    public static int[] reverseArray(int[] array) {
        int[] result=new int[array.length];
        if (array.length > 0) {
            result[0]=array[array.length-1];
            System.arraycopy(
                    reverseArray(copyOfRange(array,0,array.length-1))
                    ,0
                    ,result
                    ,1
                    ,reverseArray(copyOfRange(array,0,array.length-1)).length);
        }
        return result;
    }

    public static void printReversedArray(int[] array){
        System.out.print(Arrays.toString(reverseArray(array)));
    }

}