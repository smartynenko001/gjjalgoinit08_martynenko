package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh04N067.isWeekendDay;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh04N067Test {

    public static void main(String args[]){
        testWorkDay();
        testWeekend();
    }

    private static void testWorkDay() {
        assertEquals("Test1","Workday",isWeekendDay(5));
    }

    private static void testWeekend() {
        assertEquals("Test2","Weekend",isWeekendDay(7));
    }

}
