package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCH10N044.digitRoot;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N044Test {

    public static void main(String args[]){
        testDigitRoot();
    }

    private static void testDigitRoot(){
        assertEquals("Test Digit Root Calculation",9,digitRoot(999));
    }

}
