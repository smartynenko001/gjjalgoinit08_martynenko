package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh09N022.getFirstHalfOfTheWord;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh09N022Test {

    public static void main(String args[]){
        testGetFirstHalfOfTheWord();
    }

    private static void testGetFirstHalfOfTheWord(){
        final String TEST_WORD="Moscow";
        final String EXPECTED_RESULT="Mos";
        assertEquals(
                "Test correctness of the returned 1st half of the string"
                ,EXPECTED_RESULT
                ,getFirstHalfOfTheWord(TEST_WORD)
        );
    }

}
