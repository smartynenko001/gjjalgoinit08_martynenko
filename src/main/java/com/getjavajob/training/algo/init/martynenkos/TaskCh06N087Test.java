package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh06N087Test {

    public static void main(String Args[]){
        testResultWinner1();
        testResultWinner2();
        testResultDraw();
    }

    private static void testResultWinner1(){
        final String TEAM1_NAME="TeamA";
        final String TEAM2_NAME="TeamB";
        Game myGame=new Game(TEAM1_NAME,TEAM2_NAME);
        myGame.setCurrentScore(2,1);
        myGame.setCurrentScore(1,3);
        assertEquals("Test Result with Winner Team1:","The winner is: TeamA The looser is: TeamB The score is: 3: 1",myGame.result());
    }

    private static void testResultWinner2(){
        Game myGame=new Game("TeamA","TeamB");
        myGame.setCurrentScore(1,1);
        myGame.setCurrentScore(2,3);
        assertEquals("Test Result with Winner Team2:","The winner is: TeamB The looser is: TeamA The score is: 1: 3",myGame.result());
    }

    private static void testResultDraw(){
        Game myGame=new Game("TeamA","TeamB");
        myGame.setCurrentScore(1,1);
        myGame.setCurrentScore(2,1);
        assertEquals("Test Result with a Draw:","The game ended in a draw between TeamA and TeamB  The score is: 1: 1",myGame.result());
    }

}
