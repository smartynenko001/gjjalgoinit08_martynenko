package main.java.com.getjavajob.training.algo.init.martynenkos;

import static java.lang.Math.min;
import static sun.swing.MenuItemLayoutHelper.max;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh12N023 {

    public static int[][] taskA(int n){
            int[][] result=new int[n][n];
            for(int i=0;i<n;i++){
                result[i][i]=1;
                result[i][n-i-1]=1;
            }
            return result;
    }

    public static int[][] taskB(int n){
        int[][] result=new int[n][n];
        for(int i=0;i<n;i++){
            result[i][i]=1;
            result[i][n-i-1]=1;
            result[n/2][i]=1;
            result[i][n/2]=1;
        }
        return result;
    }

    public static int[][] taskC(int n){
        int[][] result=new int[n][n];
        for(int i=0;i<n;i++){
            for (int j=min(i,n-i-1);j<=max(n-i-1,i);j++){
                result[i][j]=1;
            }
        }
        return result;
    }

}
