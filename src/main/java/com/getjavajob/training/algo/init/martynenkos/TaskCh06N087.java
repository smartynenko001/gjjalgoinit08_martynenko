package main.java.com.getjavajob.training.algo.init.martynenkos;

import java.util.Scanner;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh06N087 {
    public static void main(String args[]){
        Game myGame=new Game();
        myGame.play();
    }
}

class Game{

    private final int[] scores;
    private final String[]  teamNames;
    private final Scanner taskScanner;

    Game(){
        this.taskScanner=new Scanner(System.in);
        this.scores=new int[2];
        this.teamNames=new String[2];
        setTeamNames();
    }

    Game(String team1Name,String team2Name) {
        this.taskScanner = new Scanner(System.in);
        this.scores = new int[2];
        this.teamNames = new String[2];
        this.teamNames[0]=team1Name;
        this.teamNames[1]=team2Name;
        }

    public void play(){
        int actionId;
        do {
            actionId=readNextAction();
            reactOnAction(actionId);
        } while (actionId>0);
        terminateGame();
    }

    private void setTeamName(int teamNumber){
        System.out.println("Enter team #"+Integer.toString(teamNumber+1));
         this.teamNames[teamNumber]=this.taskScanner.next();
    }

    private String getTeamName(int teamNumber){
        return teamNames[teamNumber-1];
    }

    private void setTeamNames(){
        for(int i=0;i<this.teamNames.length;i++){
            setTeamName(i);
        }
    }

    private int getCurrentScore(int teamNumber){
        return scores[teamNumber-1];
    }

    public void setCurrentScore(int teamNumber,int newScore){
        scores[teamNumber-1]=newScore;
    }

    private void increaseScore(int teamNumber,int score){
        setCurrentScore(teamNumber,getCurrentScore(teamNumber)+score);
    }

    private int readNextAction(){
        System.out.print("Enter team to score (1 or 2 or 0 to finish game):");
        return this.taskScanner.nextInt();
    }

    private int readNewScore(){
        System.out.print("Enter score (1 or 2 or 3):");
        return this.taskScanner.nextInt();
    }

    private void reactOnAction(int action){
        if(action>0) {
            increaseScore(action, readNewScore());
        }
    }

    private int getWinner(){
        if(getCurrentScore(1)>getCurrentScore(2)) {
            return 1;
        } else if (getCurrentScore(1)<getCurrentScore(2)) {
            return 2;
        }else {
            return 0;
        }
    }

    private int getLooser(){
        if(getCurrentScore(1)>getCurrentScore(2)) {
            return 2;
        } else if (getCurrentScore(1)<getCurrentScore(2)) {
            return 1;
        }else {
            return 0;
        }
    }

    private void printResults(){
        System.out.printf(result());
    }

    private void terminateGame(){
        printResults();
        System.out.println();
        System.out.print("Game Over!");
        this.taskScanner.close();
    }

    private String winnerPromt(){
        if(getWinner()>0) {
            return "The winner is: " + getTeamName(getWinner());
        }else {
            return "The game ended in a draw between "+getTeamName(1)+" and "+getTeamName(2);
        }
    }

    private String looserPromt(){
        if(getLooser()>0) {
            return "The looser is: " + getTeamName(getLooser());
        }else{
            return "";
        }
    }

    private String scorePrompt(){
        return "The score is: "+getCurrentScore(1)+": "+getCurrentScore(2);
    }

    public String result(){
        return winnerPromt()+" "+looserPromt()+" "+scorePrompt();
    }

}
