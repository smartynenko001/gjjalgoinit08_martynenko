package main.java.com.getjavajob.training.algo.init.martynenkos;

import java.util.Scanner;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh05N010 {
    private static final int N = 20;

    public static void main(String Args[]) {
       buildConversionTable();
    }

    private static void buildConversionTable(){
        double conversionRate=recieveConversionRate();
        double[] usdAmount=buildSequence();
        double[] rubAmount=convertUSDRUB(usdAmount,conversionRate);
        printConversionTable(usdAmount,rubAmount);
    }

    private static double recieveConversionRate(){
        System.out.println("Please enter XCCY Rate:");
        Scanner taskScanner=new Scanner(System.in);
        return taskScanner.nextDouble();
    }

    private static double[] buildSequence() {
        double[] temporarySequence = new double[N];
        for (int i = 1; i <= N-1; i++) {
            temporarySequence[i] = i;
        }
        return temporarySequence;
    }

    private static double[] convertUSDRUB(double[] dollarAmount,double conversionRate) {
        double[] temporaryArray=new double[N];
        for (int i = 1; i <= N-1; i++) {
            temporaryArray[i] = dollarAmount[i]*conversionRate;
        }
        return temporaryArray;
    }

    private static void printArray(double[] arrayForPrint) {
        for (int i = 1; i <= arrayForPrint.length-1; i++) {
            System.out.printf("%-8.2f",arrayForPrint[i]);
        }
    }

    private static void printConversionTable(double[] usdAmount,double[] rubAmount){
        System.out.print("USD Amount: ");
        printArray(usdAmount);
        System.out.println();
        System.out.print("RUB Amount: ");
        printArray(rubAmount);
    }

}
