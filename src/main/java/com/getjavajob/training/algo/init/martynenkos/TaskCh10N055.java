package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N055 {

    public static String convertDecimalToBaseN(int n,int number){
        if (number>n-1){
            return convertDecimalToBaseN(n,number/n).concat(getNumberToSymbol(number%n));
        } else{
            return getNumberToSymbol(number);
        }
    }

    private static String getNumberToSymbol(int number){
        return (number>9)? String.valueOf(Character.toChars(number+55)):String.valueOf(number);
    }

}
