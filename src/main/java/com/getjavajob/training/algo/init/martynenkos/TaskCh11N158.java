package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh11N158 {

    public static void removeDuplicates(int[] array){
        for (int i=0;i<array.length-2;i++){
            for (int j=i+1;j<array.length-1;j++){
             if (array[i]!=0&&array[j]==array[i]){
                 System.arraycopy(array,j+1,array,j,array.length-j-1);
                 j--;
                 array[array.length-1]=0;
             }
            }
        }
    }

}
