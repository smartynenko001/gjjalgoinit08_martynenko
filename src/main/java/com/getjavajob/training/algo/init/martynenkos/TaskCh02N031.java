package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh02N031 {

    public static int flipNumber(int number) {
      return getDigit(number,3)*100+getDigit(number,1)*10+getDigit(number,2);
    }

    private static int getDigit(int number, int digitPosition) {
        return (
                number %(int) Math.pow(10,digitPosition)- number % (int) Math.pow(10,digitPosition-1)
               )
                /
                (int)Math.pow(10,digitPosition-1);
    }

}
