package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh10N043.getSumOfDigits;

/**
 * Created by Sergey Martynenko.
 */
class TaskCH10N044 {

    public static int digitRoot(int number){
        return (number>9)? digitRoot(getSumOfDigits(number)):number;
    }

}
