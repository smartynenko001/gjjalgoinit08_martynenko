package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh10N052.getInvertedNumber;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N052Test {

    public static void main(String args[]){
        testGetInvertedNumber();
    }

    private static void testGetInvertedNumber(){
        assertEquals("Test inverted number","731",getInvertedNumber(137));
    }

}
