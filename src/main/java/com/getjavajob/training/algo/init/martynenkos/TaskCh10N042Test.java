package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh10N042.power;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N042Test {

    public static void main(String args[]){
        testPower();
    }

    private static void testPower(){
        assertEquals("Test power algorithm",1024,power(2,10));
    }

}
