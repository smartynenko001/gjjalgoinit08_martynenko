package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh10N050.Akkerman;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N050Test {

    public static void main(String[] args){
        System.out.println(Akkerman(1,3));
        testAkkerman();
    }

    private static void testAkkerman(){
        assertEquals("Test Akkerman Function",13,Akkerman(3,1));
    }

}
