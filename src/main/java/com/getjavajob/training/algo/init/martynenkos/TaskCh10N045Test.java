package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCH10N045.nSumArithmetic;
import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCH10N045.nthTermArithmetic;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N045Test {

    public static void main(String args[]){
        testNSumArithmetic();
        testNthMemberArithmetic();
    }

    private static void testNthMemberArithmetic(){
        assertEquals("Test nth Arithmetic Term Calculation",20, nthTermArithmetic(2,2,10));
    }

    private static void testNSumArithmetic(){
        assertEquals("Test nth Arithmetic Sum Calculation",110,nSumArithmetic(2,2,10));
    }

}
