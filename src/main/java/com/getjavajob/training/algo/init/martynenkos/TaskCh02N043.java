package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh02N043 {

    public static int isPossibleToDivide(int a,int b) {
        return Boolean.compare((a!=0 && b%a==0) || (b!=0 && a%b==0),true)+1;
    }

}
