package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh04N106 {

    public static String assertSeasonByMonthNumber(int monthNumber){
        switch (monthNumber) {
            case 1:
            case 2:
            case 12:
                return "Winter";
            case 3:
            case 4:
            case 5:
                return "Spring";
            case 6:
            case 7:
            case 8:
                return "Summer";
            default:
                return "Autumn";
        }
    }

}
