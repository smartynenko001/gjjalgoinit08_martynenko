package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh09N166.swapWords;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh09N166Test {

    public static void main(String args[]){
        testSwapWords();
    }

    private static void testSwapWords(){
        final String TEST_SENTENCE="word1 word2 word3";
        final String EXPECTED_RESULT="word3 word2 word1";
        assertEquals("Test if words are swapped correctly",EXPECTED_RESULT,swapWords(TEST_SENTENCE).toString());
    }

}
