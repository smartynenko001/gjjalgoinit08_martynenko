package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh03N029.*;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh03N029Test {

    public static void main(String args[]) {
        testTaskA();
        testTaskB();
        testTaskV();
        testTaskG();
        testTaskD();
        testTaskE();
    }

    private static void testTaskA() {
        final int X=4;
        final int Y=100;
        final boolean EXPECTED_RESULT=true;
        assertEquals("TaskCh03N029Test.TaskA",EXPECTED_RESULT,taskA(X,Y));
    }

    private static void testTaskB() {
        final int X=4;
        final int Y=100;
        final boolean EXPECTED_RESULT=true;
        assertEquals("TaskCh03N029Test.TaskB",EXPECTED_RESULT,taskB(X,Y));
    }

    private static void testTaskV() {
        final int X=0;
        final int Y=100;
        final boolean EXPECTED_RESULT=true;
        assertEquals("TaskCh03N029Test.TaskV",EXPECTED_RESULT,taskV(X,Y));
    }

    private static void testTaskG() {
        final int X=-3;
        final int Y=-2;
        final int Z=-3;
        final boolean EXPECTED_RESULT=true;
        assertEquals("TaskCh03N029Test.TaskG",EXPECTED_RESULT,taskG(X,Y,Z));
    }

    private static void testTaskD() {
        final int X=5;
        final int Y=101;
        final int Z=21;
        final boolean EXPECTED_RESULT=true;
        assertEquals("TaskCh03N029Test.TaskD",EXPECTED_RESULT,taskD(X,Y,Z));
    }

    private static void testTaskE() {
        final int X=0;
        final int Y=100;
        final int Z=300;
        final boolean EXPECTED_RESULT=true;
        assertEquals("TaskCh03N029Test.TaskE",EXPECTED_RESULT,taskE(X,Y,Z));
    }

}
