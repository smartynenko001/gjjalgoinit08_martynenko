package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh04N033 {

    public static boolean isEven(int number) {
        return number%2==0;
    }

    public static boolean isOdd(int number) {
        return !isEven(number);
    }

}
