package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh02N039 {

    public static double getHourHandAngle(int hours,int minutes,int seconds){
        return 30*hours%12+minutes/2+seconds/120;
    }

}
