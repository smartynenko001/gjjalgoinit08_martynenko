package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh12N063.averageStudentsPerClass;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh12N063Test {

    public static void main(String args[]){
        testAverageStudentsPerClass();
    }

    private static void testAverageStudentsPerClass(){
        int[][] TEST_SCHOOL={
                {30,30,28,28},
                {30,30,28,28},
                {30,30,28,28},
                {30,30,28,28},
                {30,30,28,28},
                {30,30,28,28},
                {30,30,28,28},
                {30,30,28,28},
                {30,30,28,28},
                {30,30,28,28},
                {30,30,28,28}

        };
        int[] RESULT_AVERAGE={
                29,
                29,
                29,
                29,
                29,
                29,
                29,
                29,
                29,
                29,
                29
        };
        assertEquals("Test average students per class calculation:",RESULT_AVERAGE,averageStudentsPerClass(TEST_SCHOOL));
    }

}
