package main.java.com.getjavajob.training.algo.init.martynenkos;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh04N036 {

    private static final int GREEN_TIME=3;

    private static final int RED_TIME=2;

    public static String assertTrafficLightColor(double time) {
        return (time%(GREEN_TIME+RED_TIME)>=GREEN_TIME) ? "Red" : "Green";

    }

}
