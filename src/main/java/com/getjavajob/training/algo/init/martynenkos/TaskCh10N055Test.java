package main.java.com.getjavajob.training.algo.init.martynenkos;

import static main.java.com.getjavajob.training.algo.init.martynenkos.TaskCh10N055.convertDecimalToBaseN;
import static main.java.com.getjavajob.training.algo.util.Assert.assertEquals;

/**
 * Created by Sergey Martynenko.
 */
class TaskCh10N055Test {

    public static void main(String args[]){
        testConvertDecimalToBaseN();
    }

    private static void testConvertDecimalToBaseN(){
        final int TEST_NUMBER=555;
        final int NEW_BASE=15;
        final String EXPECTED_NUMBER="270";
        assertEquals("Test BaseN Conversion of a Decimal Number"
                ,EXPECTED_NUMBER,convertDecimalToBaseN(NEW_BASE,TEST_NUMBER));
    }

}
